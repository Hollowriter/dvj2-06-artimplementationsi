﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(AudioSource))]
public class PlayAudioSourceExample : MonoBehaviour
{
	private void Start ()
	{
		AudioSource myAudioSource = GetComponent<AudioSource> ();
		myAudioSource.Play ();
	}
}
